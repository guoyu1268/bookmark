document.addEventListener('DOMContentLoaded', function () {
	chrome.tabs.query({'currentWindow': true, 'active':true}, function(t){
		$('#title').val(t[0].title);
		$('#url').val(t[0].url);
	});

	$.getJSON("http://bookmark.xuguoyu.com/label", function(result){
		if (result.code == 0) {
			var e_label = $('#label');
			for (var k in  result.list) {
				e_label.append('<option value="' + k + '">' + result.list[k] + '</option>');
			}
		}
	});
	
	var msg = $('span.msg'), submitBtn = $('button[type="submit"]');
	$("input[name='content']").val( decodeURIComponent(location.hash.slice(9)) );
	$('#addForm').ajaxForm({
		dataType: 'json',
		beforeSubmit: function() {
			msg.html('<span class="label label-info">正在保存。。。</span>');
			submitBtn.attr('disabled', 'disabled');
		},
		success: function(data) {
			submitBtn.removeAttr('disabled');
			if(data.code != 0){
				msg.html('<span class="label label-danger">' + data.msg + '</span>');
				setTimeout(function(){
					msg.html('');
				}, 3000)
			}else{
				msg.html('<span class="label label-success">success</span>');
				setTimeout(function(){
					window.close();
				}, 1000);
			}
		},
		error: function() {
			msg.html('<span class="label label-danger">保存失败！</span>');
			submitBtn.removeAttr('disabled');
		}
	});
	$('#summary').focus();
});
