<%@page import="com.bookmark.common.Config"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.bookmark.dao.BookmarkDaoImpl"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.bookmark.pojo.Bookmark"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add - Bookmark</title>
<link rel="stylesheet" href="<%=Config.host %>/static/bootstrap/css/bootstrap.css">
<style>
body{background-color: #333}
h2.title{color: #fff;padding: 20px 0;}
.content{width: 44em;position: absolute;top: 50%;left: 50%;margin-left: -22em;margin-top: -16em;}
.content h2{text-align: center;}
.content label{color: #999}
span.msg{margin-left: 20px;}
</style>
</head>
<body>
<div class="content">
  <h2 class="title">添加书签</h2>
  <form id="addForm" class="form-horizontal" action="<%=Config.host %>/save" method="post">
    <div class="form-group">
      <label for="label" class="col-sm-2 control-label">类别</label>
      <div class="col-sm-10">
        <select class="form-control" id="label" name="label">
          <option value="0">默认</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="title" class="col-sm-2 control-label">标题</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" value="" id="title" name="title" placeholder="标题" required>
      </div>
    </div>
    <div class="form-group">
      <label for="url" class="col-sm-2 control-label">链接</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" value=""  id="url" name="url" placeholder="链接" required>
      </div>
    </div>
    <div class="form-group">
      <label for="summary" class="col-sm-2 control-label">摘要</label>
      <div class="col-sm-10">
        <textarea style="height:100px;" class="form-control" id="summary" name="summary" placeholder="摘要"></textarea>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">SAVE</button>
        <span class="msg"></span>
      </div>
    </div>
  </form>
</div>
<script src="<%=Config.host %>/static/jquery/jquery-2.1.4.js"></script>
<script src="<%=Config.host %>/static/jquery/jquery.form.js"></script>
<script>
$(function(){
  $.getJSON("<%=Config.host %>/label", function(result){
	  if (result.code == 0) {
		  var e_label = $('#label');
		  for (var k in  result.list) {
			  e_label.append('<option value="' + k + '">' + result.list[k] + '</option>');
		  }
	  }
  });
  var msg = $('span.msg'), submitBtn = $('button[type="submit"]');
  $("input[name='content']").val( decodeURIComponent(location.hash.slice(9)) );
  $('#addForm').ajaxForm({
	dataType: 'json',
    beforeSubmit: function() {
      msg.html('<span class="label label-info">正在保存。。。</span>');
      submitBtn.attr('disabled', 'disabled');
    },
    success: function(data) {
      submitBtn.removeAttr('disabled');
      if(data.code != 0){
        msg.html('<span class="label label-danger">' + data.msg + '</span>');
        setTimeout(function(){
          msg.html('');
        }, 3000)
      }else{
        msg.html('<span class="label label-success">success</span>');
        setTimeout(function(){
        	window.location.href = '<%=Config.host %>/'
        }, 1000);
      }
    },
    error: function() {
      msg.html('<span class="label label-danger">保存失败！</span>');
      submitBtn.removeAttr('disabled');
    }
  });
  $('#summary').focus();
})
</script>
</body>
</html>