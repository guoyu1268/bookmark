<%@page import="com.bookmark.common.Config"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.bookmark.dao.BookmarkDaoImpl"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.bookmark.pojo.Bookmark"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
// 获取参数
int id = 0;
String readType = "terse";
if (request.getParameter("id") != null) {
    id = Integer.parseInt(request.getParameter("id"));
}
if (request.getParameter("terse") != null) {
    readType = request.getParameter("terse"); // todo type:简洁、丰富
}
// 读DB
Bookmark bookmark = new Bookmark();
bookmark.setId(id);
BookmarkDaoImpl bookmarkDaoImpl = new BookmarkDaoImpl();
bookmark = bookmarkDaoImpl.getDetail(bookmark);
bookmarkDaoImpl.connClose();

String link = bookmark.getLink();
String shortLink = link.length() > 50 ? (link.substring(0, 50) + " …") : link;
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><%=bookmark.getTitle() %> - Bookmark</title>
  <link href="<%=Config.host %>/static/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link rel="icon" href="<%=Config.host %>/static/icon.png">
  <style>
    body {padding-top: 50px;}
    .navbar-left {padding-left: 80px; font-size: 18px;}
    .navbar-right {padding-right: 80px; font-size: 18px;}
    .navbar-center { position: absolute; left: 50%; transform: translatex(-50%);}
    .container {margin-bottom: 50px; max-width: 800px; font-family: LyonText,Georgia,serif; line-height: 1.7em; font-size: 18px;}
    .page-header>a {font-size: 18px; color: #999;}
    .content {color: #000;}
  </style>
</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>    
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
          <li><a class="glyphicon glyphicon-chevron-left" href="javascript:history.back()"></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-center">
          <li><a><strong>Bookmark</strong></a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a class="glyphicon glyphicon-link" target="_blank" href="<%=link %>"></a></li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
    <div class="page-header">
      <h1><strong><%=bookmark.getTitle() %></strong></h1>
      <a target="_blank" href="<%=link %>"><%=shortLink %></a>
    </div>
    <div class="content">
      <%=bookmark.getSnapshot() %>
    </div>
  </div>
</body>
</html>