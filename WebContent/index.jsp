<%@page import="com.bookmark.common.Config"%>
<%@page import="com.bookmark.common.PageBean"%>
<%@page import="com.bookmark.pojo.Bookmark"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>List - Bookmark</title>
  <link href="<%=Config.host %>/static/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link rel="icon" href="<%=Config.host %>/static/icon.png">
  <style>
    body {padding-top: 50px;}
    .navbar-fixed-top {border: 0;}
    @media (min-width: 768px) {
      .sidebar {position: fixed;top: 51px;bottom: 0;left: 0;z-index: 1000;display: block;padding: 20px;overflow-x: hidden;overflow-y: auto; background-color: #f5f5f5;border-right: 1px solid #eee;}
    }
    .nav-sidebar {margin-right: -21px; /* 20px padding + 1px border */margin-bottom: 20px;margin-left: -20px;}
    .nav-sidebar > li > a {padding-right: 20px;padding-left: 20px;}
    .nav-sidebar > .active > a,.nav-sidebar > .active > a:hover, .nav-sidebar > .active > a:focus {color: #fff;background-color: #428bca;}
    .main {padding: 20px;}
    @media (min-width: 768px) {
      .main {padding-right: 40px;padding-left: 40px;}
    }
    .main .page-header {margin-top: 0;}

    .navbar-bookmark {background-color: #428bca;}
    .article-title {margin-top: 10px; margin-bottom: 5px;}
    .article-linkout {margin-top: 5px;}
    .article-title a{text-decoration: none; color: #222; font-size: 20px;}
    .article-linkout a{text-decoration: none; color: #7a7a7a; font-size: 16px;}
    .article-preview {margin-top: 5px; color: #404040; font-family: LyonText,Georgia,serif; font-size: 16px; text-overflow: ellipsis; overflow: hidden; white-space: normal;}
    .article-meta {margin-top: 5px; margin-bottom: 10px; color: #7a7a7a; font-size: 14px;}
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-fixed-top navbar-bookmark">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="color: #FFF;" href="/">Bookmark</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <!--ul class="nav navbar-nav navbar-right">
          <li><a style="color: #FFF;" href="/">Home</a></li>
          <li><a style="color: #FFF;" href="#">Settings</a></li>
          <li><a style="color: #FFF;" href="#">Profile</a></li>
        </ul-->
        <form class="navbar-form navbar-right">
          <input type="text" class="form-control" placeholder="Search">
        </form>
      </div>
    </div>
  </nav>
  
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-3 col-md-2 sidebar">
        <% int label = (int)request.getAttribute("label"); %>
        <ul class="nav nav-sidebar">
          <li class="<%=label == -1 ? "active" : "" %>"><a href="<%=Config.host %>/?label=-1">全部</a></li>
          <li class="<%=label == 1 ? "active" : "" %>"><a href="<%=Config.host %>/?label=1">技术</a></li>
          <li class="<%=label == 2 ? "active" : "" %>"><a href="<%=Config.host %>/?label=2">生活</a></li>
        </ul>
      </div>
      <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <!--h4 class="page-header">List</h4-->
        <div class="table-responsive">
          <table class="table">
            <tbody>
              <%
              PageBean<Bookmark> pageBean = (PageBean<Bookmark>)request.getAttribute("pageBean");
              Bookmark bm = null;
              for (int i = 0; i < pageBean.getRecordList().size(); i ++) {
                  bm = pageBean.getRecordList().get(i);
                  String link = bm.getLink();
                  String shortLink = link.length() > 80 ? (link.substring(0, 80) + " …") : link;
              %>
              <tr>
                <td>
                  <div class="article-title">
                    <a href="<%=Config.host %>/read.jsp?id=<%=bm.getId() %>" title="<%=bm.getTitle() %>"><%=bm.getTitle() %></a>
                  </div>
                  <div class="article-linkout">
                    <a href="<%=link %>" target="_blank"><%=shortLink %></a>
                  </div>
                  <div class="article-preview"><%=bm.getSummary() %> …</div>
                  <div class="article-meta"><%=bm.getAddtime() %></div>
                </td>
              </tr>
              <%} %>
            </tbody>
          </table>
        </div>

        <nav>
          <ul class="pager">
            <%if (pageBean.getPage() > 1) { %>
            <li><a href="<%=Config.host %>/?label=<%=label %>&page=<%=pageBean.getPrivPage() %>">上一页</a></li>
            <%} else { %>
            <li class="disabled"><a href="javascript:void(0)">上一页</a></li>
            <%} %>
            <li class="current disabled"><a href="<%=Config.host %>/?label=<%=label %>&page=${requestScope.pageBean.getPage() }">${requestScope.pageBean.getPage() }</a></li>
            <!--li><a href="/?page=6">6</a></li>
            <li><a href="/?page=7">7</a></li>
            <li class="disabled"><span>...</span></li>
            <li><a href="/?page=61">61</a></li-->
            <%if (pageBean.getPage() < pageBean.getTotalPage()) { %>
            <li><a href="<%=Config.host %>/?label=<%=label %>&page=<%=pageBean.getNextPage() %>">下一页</a></li>
            <%} %>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</body>
</html>