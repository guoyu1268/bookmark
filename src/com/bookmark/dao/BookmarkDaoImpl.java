package com.bookmark.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bookmark.common.PageBean;
import com.bookmark.dbc.Mysql;
import com.bookmark.pojo.Bookmark;

public class BookmarkDaoImpl implements BookmarkDao {
    private Connection conn = null; // 定义数据库连接对象
    private PreparedStatement pstmt = null; // 定义数据库操作对象
    private ResultSet res = null; // 定义数据库结果对象

    public BookmarkDaoImpl() {
        try {
            this.conn = Mysql.conn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void connClose() {
        try {
            Mysql.close(res, pstmt, conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int insert(Bookmark bookmark) throws SQLException {
        int id = 0;
        try {
            String sql = "insert into bookmark (`title`, `link`, `label`, `summary`, `add_time`) values (?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, bookmark.getTitle());
            pstmt.setString(2, bookmark.getLink());
            pstmt.setInt(3, bookmark.getLabelid());
            pstmt.setString(4, bookmark.getSummary());
            pstmt.setString(5, bookmark.getAddtime());
            pstmt.executeUpdate();

            ResultSet rs = pstmt.executeQuery("SELECT last_insert_id();");
            if (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            throw e;
        }
        return id;
    }

    @Override
    public void addsnapshot(Bookmark bookmark) throws SQLException {
        try {
            String sql = "insert into snapshot (`bookmark`, `content`, `add_time`) values (?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, bookmark.getId());
            pstmt.setString(2, bookmark.getSnapshot());
            pstmt.setString(3, bookmark.getAddtime());
            pstmt.executeUpdate();
        } catch (Exception e) {
            throw e;
        }

    }

    @Override
    public void update(Bookmark bookmark) throws SQLException {
        try {
            String sql = "update bookmark set `title`=?, `summary`=? where id=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, bookmark.getTitle());
            pstmt.setString(2, bookmark.getSummary());
            pstmt.setInt(3, bookmark.getId());
            pstmt.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Bookmark> getList(int offset, int limit, int label) throws SQLException {
        List<Bookmark> bookmarks = new ArrayList<Bookmark>();
        try {
            String where = "";
            if (label > -1) {
                where = String.format("where `label`=%d", label);
            }
            String sql = String.format("select * from bookmark %s order by id desc limit %d, %d", where, offset, limit);
            pstmt = conn.prepareStatement(sql);
            res = pstmt.executeQuery();
            while (res.next()) {
                Bookmark bookmark = new Bookmark();
                bookmark.setId(res.getInt("id"));
                bookmark.setTitle(res.getString("title"));
                bookmark.setLink(res.getString("link"));
                bookmark.setLabelid(res.getInt("label"));
                bookmark.setSummary(res.getString("summary"));
                bookmark.setStatus(res.getInt("status"));
                bookmark.setAddtime(res.getString("add_time"));
                bookmarks.add(bookmark);
            }
        } catch (Exception e) {
            throw e;
        }
        return bookmarks;
    }

    @Override
    public int getListCount() throws SQLException {
        int count = 0;
        try {
            String sql = "select count(*) as count from bookmark";
            pstmt = conn.prepareStatement(sql);
            res = pstmt.executeQuery();
            if (res.next()) {
                count = res.getInt("count");
            }
        } catch (Exception e) {
            throw e;
        }
        return count;
    }

    @Override
    public PageBean<Bookmark> listPage(int page, int pageSize) {
        return new PageBean<Bookmark>(page, pageSize);
    }

    @Override
    public Bookmark getDetail(Bookmark bookmark) throws SQLException {
        try {
            String sql = "select b.*, s.content from bookmark as b left join snapshot as s on s.bookmark=b.id where b.id=?";
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, bookmark.getId());
            res = pstmt.executeQuery();
            if (res.next()) {
                bookmark.setTitle(res.getString("title"));
                bookmark.setLink(res.getString("link"));
                bookmark.setLabelid(res.getInt("label"));
                bookmark.setSummary(res.getString("summary"));
                bookmark.setStatus(res.getInt("status"));
                bookmark.setAddtime(res.getString("add_time"));
                bookmark.setSnapshot(res.getString("content"));
            }
        } catch (Exception e) {
            throw e;
        }
        return bookmark;
    }

    @Override
    public Map<Integer, String> getLabelList() throws SQLException {
        Map<Integer, String> list = new HashMap<Integer, String>();
        try {
            String sql = "select * from `label` where status=0 order by id";
            pstmt = conn.prepareStatement(sql);
            res = pstmt.executeQuery();
            while (res.next()) {
                list.put(res.getInt("id"), res.getString("name"));
            }
        } catch (Exception e) {
            throw e;
        }
        return list;
    }
}
