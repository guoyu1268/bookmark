package com.bookmark.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.bookmark.common.PageBean;
import com.bookmark.pojo.Bookmark;

public interface BookmarkDao {
    public int insert(Bookmark bookmark) throws SQLException;

    public void addsnapshot(Bookmark bookmark) throws SQLException;

    public void update(Bookmark bookmark) throws SQLException;

    public List<Bookmark> getList(int offset, int limit, int label) throws SQLException;

    public int getListCount() throws SQLException;

    public Bookmark getDetail(Bookmark bookmark) throws SQLException;

    public Map<Integer, String> getLabelList() throws SQLException;

    // 放这里合适吗
    public PageBean<Bookmark> listPage(int page, int pageSize);

}
