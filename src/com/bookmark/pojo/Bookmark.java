package com.bookmark.pojo;

public class Bookmark { //implements java.io.Serializable // 有些框架下，不实现Serializable会报错
    //private static final long serialVersionUID = 9065313803532254801L;
    
    private int id;
    private int labelid;
    private String link;
    private String title;
    private String summary;
    private int status;
    private String addtime;
    private String snapshot;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getLabelid() {
        return labelid;
    }
    public void setLabelid(int labelid) {
        this.labelid = labelid;
    }
    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getAddtime() {
        return addtime;
    }
    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }
    public String getSnapshot() {
        return snapshot;
    }
    public void setSnapshot(String snapshot) {
        this.snapshot = snapshot;
    }
}
