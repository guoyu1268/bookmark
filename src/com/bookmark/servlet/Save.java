package com.bookmark.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookmark.dao.BookmarkDaoImpl;
import com.bookmark.pojo.Bookmark;

/**
 * Servlet implementation class Save
 */
@WebServlet("/Save")
public class Save extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Save() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // init response
        int respCode = 0;
        String respMsg = "";

        // 获取参数
        int label = 0;
        String title = "", url = "", summary = "";
        if (request.getParameter("label") != null) {
            try {
                label = Integer.parseInt(request.getParameter("label"));
            } catch (Exception e) {
            }
        }
        if (request.getParameter("title") != null) {
            title = request.getParameter("title");
        }
        if (request.getParameter("url") != null) {
            url = request.getParameter("url");
        }
        if (request.getParameter("summary") != null) {
            summary = request.getParameter("summary");
        }
        try {
            if (title.length() < 3 || url.length() < 8 || summary.length() < 20 || label < 0 || label > 99999) {
                throw new Exception("参数错误");
            }
            // 获取快照
            String snapshot = "";
            URL urlObj = new URL("http://mercury.postlight.com/parser?url=" + URLEncoder.encode(url, "utf-8"));
            HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
            //conn.setRequestProperty("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
            conn.setRequestProperty("x-api-key", "LRKIXrEh8BmEd6NzgtEQCIbK0mPEWqYuLEAzHr9e");
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(9000);
            if (conn.getResponseCode() == 200) {
                BufferedReader buff = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String jsonStr = "", line = "";
                while ((line = buff.readLine()) != null) {
                    jsonStr += line + "\n";
                }
                buff.close();
                JSONObject urlResult = new JSONObject(jsonStr);
                snapshot = urlResult.getString("content");
            }
            conn.disconnect();

            // 当前时间
            Date nowTime = new Date();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Bookmark bookmark = new Bookmark();
            bookmark.setTitle(title);
            bookmark.setLink(url);
            bookmark.setSummary(summary);
            bookmark.setLabelid(label);
            bookmark.setSnapshot(snapshot);
            bookmark.setAddtime(df.format(nowTime));

            // 写DB
            BookmarkDaoImpl bookmarkDaoImpl = new BookmarkDaoImpl();
            int id = bookmarkDaoImpl.insert(bookmark);
            bookmark.setId(id);
            bookmarkDaoImpl.addsnapshot(bookmark);
            bookmarkDaoImpl.connClose();
        } catch (Exception e) {
            e.printStackTrace();
            respCode = 1;
            respMsg = e.getMessage();
        }

        JSONObject resp = new JSONObject();
        resp.put("code", respCode);
        resp.put("msg", respMsg);
        response.setCharacterEncoding("utf-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.getWriter().append(resp.toString());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
