package com.bookmark.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.bookmark.dao.BookmarkDaoImpl;

/**
 * Servlet implementation class Label
 */
@WebServlet("/Label")
public class Label extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Label() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    int respCode = 0;
	    String respMsg = "";
	    Map <Integer, String> list = new HashMap<Integer, String>();
	    
	    try {
	        BookmarkDaoImpl bookmarkDaoImpl = new BookmarkDaoImpl();
	        list = bookmarkDaoImpl.getLabelList();
	    } catch (Exception e) {
            e.printStackTrace();
            respCode = 1;
            respMsg = e.getMessage();
        }
	    
	    JSONObject resp = new JSONObject();
        resp.put("code", respCode);
        resp.put("msg", respMsg);
        resp.put("list", list);
        response.setCharacterEncoding("utf-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.getWriter().append(resp.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
