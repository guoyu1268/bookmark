package com.bookmark.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookmark.common.PageBean;
import com.bookmark.dao.BookmarkDaoImpl;
import com.bookmark.pojo.Bookmark;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 获取参数
        int label = -1, page = 1;
        if (request.getParameter("label") != null) {
            try {
                label = Integer.parseInt(request.getParameter("label"));
            } catch (Exception e) {
            }
        }
        if (label < -1 || label > 99999) {
            label = -1;
        }
        if (request.getParameter("page") != null) {
            try {
                page = Integer.parseInt(request.getParameter("page"));
            } catch (Exception e) {
            }
        }
        if (page < 1) {
            page = 1;
        }

        // bookmark
        BookmarkDaoImpl bookmarkDaoImpl = new BookmarkDaoImpl();
        List<Bookmark> bookmarks = new ArrayList<Bookmark>();

        // 分页
        int pageSize = 10;
        PageBean<Bookmark> pageBean = bookmarkDaoImpl.listPage(page, pageSize);
        pageBean.setUri(request.getRequestURI());
        pageBean.setQuery(request.getQueryString());

        // 读DB
        try {
            bookmarks = bookmarkDaoImpl.getList(pageBean.getOffset(), pageBean.getPageSize(), label);
            pageBean.setRecordList(bookmarks);
            pageBean.setCountRecord(bookmarkDaoImpl.getListCount());
            bookmarkDaoImpl.connClose();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        response.setCharacterEncoding("utf-8");
        request.setAttribute("pageBean", pageBean);
        request.setAttribute("label", label);
        request.getRequestDispatcher("index.jsp").forward(request, response);
        // response.getWriter().append("Served at:
        // ").append(request.getContextPath()).append("\nTitle: " + title);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}
