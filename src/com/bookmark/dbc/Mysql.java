package com.bookmark.dbc;

import java.sql.*;

/**
 * 数据库操作工具类(静态类)
 * @author ricky
 *
 */
public class Mysql {
	private static String JDBC_DRIVER;
	private static String USER;
	private static String PASS;
	private static String DB;
	private static String CHARSET;
	private static String DB_URL;
    
    // 工具类，一般不要实例化，此时可以采用单例设计模式，或者将构造方法私有化
    private Mysql() {}
    
    // 静态代码块，只会在类加载的时候执行一次
    static {
        JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    	    USER        = "root";
    	    PASS        = "12345678";
    	    DB          = "bookmark";
    	    CHARSET     = "utf-8";
    	    DB_URL      = String.format("jdbc:mysql://localhost:3306/%s?characterEncoding=%s&autoReconnect=true&useSSL=false", DB, CHARSET);
    	    try {
    	    		Class.forName(JDBC_DRIVER);
    	    } catch (Exception e) {
    	    		e.printStackTrace();
    	    }
    }
    
    public static Connection conn() throws SQLException {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return connection;
    }
    
    public static void close(ResultSet res, Statement stmt, Connection connection) throws SQLException {
        try {
        		if (res != null) res.close();
        		if (stmt != null) stmt.close();
            if (connection != null) connection.close();
        } catch (SQLException e) {
            throw e;
        }
    }
    
    // jdbc完整demo
    public void jdbcDemo() {
        Connection conn = null;
        Statement stmt = null;
        try {
            // 注册JDBC驱动
            Class.forName(JDBC_DRIVER);
            
            System.out.println("- 创建数据库链接");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            
            System.out.println("- 实例化Statement对象");
            stmt = conn.createStatement();
            String sql;
            sql = "select * from bookmark";
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                int id = rs.getInt("id");
                String link = rs.getString("link");
                String title = rs.getString("title");
                
                System.out.println(id + ". " + link + "\t" + title);
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                // TODO: handle exception
            }
        }
        System.out.println("- Done!");
    }
}
