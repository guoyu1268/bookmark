package com.bookmark.dbc;

import java.sql.*;

public class Mysql2 {
	private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER        = "root";
	private static final String PASS        = "12345678";
	private static final String DB          = "bookmark";
	private static final String CHARSET     = "utf-8";
	private static final String DB_URL      = String.format("jdbc:mysql://localhost:3306/%s?characterEncoding=%s&autoReconnect=true&useSSL=false", DB, CHARSET);
    
    private Connection connection = null;
    
    public Mysql2() throws SQLException {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (Exception e) {
        		e.printStackTrace();
        }
    }
    
    public Connection conn() throws SQLException {
        if (connection == null) {
            try {
                this.connection = DriverManager.getConnection(DB_URL, USER, PASS);
            } catch (SQLException e) {
                e.printStackTrace();
                throw e;
            }
        }
        return connection;
    }
    
    public void close() throws SQLException {
        try {
            if (connection != null) connection.close();
        } catch (SQLException e) {
            throw e;
        }
    }
    
    // jdbc完整demo
    public void jdbcDemo() {
        Connection conn = null;
        Statement stmt = null;
        try {
            // 注册JDBC驱动
            Class.forName(JDBC_DRIVER);
            
            System.out.println("- 创建数据库链接");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            
            System.out.println("- 实例化Statement对象");
            stmt = conn.createStatement();
            String sql;
            sql = "select * from bookmark";
            ResultSet rs = stmt.executeQuery(sql);
            
            while (rs.next()) {
                int id = rs.getInt("id");
                String link = rs.getString("link");
                String title = rs.getString("title");
                
                System.out.println(id + ". " + link + "\t" + title);
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                // TODO: handle exception
            }
        }
        System.out.println("- Done!");
    }
}
