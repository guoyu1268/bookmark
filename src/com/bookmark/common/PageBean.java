package com.bookmark.common;

import java.io.Serializable;
import java.util.List;

public class PageBean<T> implements Serializable {
    private static final long serialVersionUID = 5766795267306170443L;
    
    private int page; // 当前页码
    private int pageSize; // 每页条数
    private int offset; // 数据库起始值
    
    private int countRecord; // 总条数
    private int totalPage; // 总页数
    private int nextPage; // 下一页
    private int privPage; // 上一页
    
    private List<T> recordList; // 本页数据列表
    
    private String uri; // 当前请求的URI
    private String query; // 当前请求的参数
    
    /**
     * 无参构造方法
     */
    public PageBean() {}
    
    /**
     * 构造方法
     */
    public PageBean(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
        
        // 计算offset
        offset = (this.page - 1) * this.pageSize;
        
        this.nextPage = this.page + 1;
        this.privPage = this.page - 1;
        if (this.privPage < 1) {
            this.privPage = 1;
        }
    }

    /**
     * 计算总页数
     */
    public static int countTotalPage(int countRecord, int pageSize) {
        if (countRecord % pageSize == 0) {
            return countRecord / pageSize;
        } else {
            return countRecord / pageSize + 1;
        }
    }
    
    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }
    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    public int getCountRecord() {
        return countRecord;
    }
    public void setCountRecord(int countRecord) {
        this.countRecord = countRecord;
        // 计算总页数
        totalPage = PageBean.countTotalPage(countRecord, pageSize);
        // 检查下一页值范围
        if (nextPage > totalPage) {
            nextPage = totalPage;
        }
    }
    public int getTotalPage() {
        return totalPage;
    }
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
    public List<T> getRecordList() {
        return recordList;
    }
    public void setRecordList(List<T> recordList) {
        this.recordList = recordList;
    }
    public int getOffset() {
        return offset;
    }
    public void setOffset(int offset) {
        this.offset = offset;
    }
    public int getNextPage() {
        return nextPage;
    }
    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }
    public int getPrivPage() {
        return privPage;
    }
    public void setPrivPage(int privPage) {
        this.privPage = privPage;
    }
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }
    public String getQuery() {
        return query;
    }
    public void setQuery(String query) {
        this.query = query;
    }
}
